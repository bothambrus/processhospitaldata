#################################################################################
#   ProcessHospitalData                                                         #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import json
import os

from processhospitaldata.output import formatList


def processDataFile(
    input_path,
    output_path,
    filter_col,
    filter_vals,
    header,
    separator,
    get_unique=False,
    skiprows=1,
    maxlines=1e10,
    printInterval=250000,
):
    """
    Process the data file `input_path` line by line, and write the matching lines to `output_path`.
    """
    g = open(output_path, "w")

    #
    # Add header to output file
    #
    line = ""
    for ih, h in enumerate(header):
        if ih == len(header) - 1:
            line += "{}".format(h)
        else:
            line += "{}{}".format(h, separator)
    line += "\n"
    g.write(line)

    #
    # Start processing
    #
    iline = 0
    imatch = 0
    line = True

    count = {}
    for v in filter_vals:
        count[v] = 0

    if get_unique:
        count_unique = {}

    #
    # Process
    #
    with open(input_path, "r") as f:
        for ii in range(skiprows):
            line = f.readline()
        while line:
            #
            # Read
            #
            line = f.readline()
            if separator == "":
                data = line.split()
            else:
                data = line.split(separator)
            iline += 1

            #
            # Identify matches
            #
            if len(data) > filter_col:
                #
                # Extract matching lines
                #
                for fv in filter_vals:
                    l = len(fv)
                    if (len(data[filter_col]) >= l) and (data[filter_col][0:l] == fv):

                        imatch += 1
                        count[fv] += 1
                        g.write(line)

                #
                # Count unique options if requested
                #
                if get_unique:
                    if data[filter_col] in count_unique.keys():
                        count_unique[data[filter_col]] += 1
                    else:
                        count_unique[data[filter_col]] = 1

            #
            # Print progress
            #
            if iline % printInterval == 0:
                print("Processed {} lines".format(iline))

            #
            # Exit if too many lines have been processed
            #
            if iline > maxlines:
                break

    #
    # Print results
    #
    print("Found {} matches for the requested filtering".format(imatch))

    stats = []
    for v in filter_vals:
        stats.append("{}: {}".format(v, count[v]))
    print(formatList(stats))

    #
    # Save unique count
    #
    if get_unique:
        with open(
            os.path.join(
                os.path.dirname(output_path),
                "{}_count_col{}.json".format(
                    os.path.basename(output_path), filter_col + 1
                ),
            ),
            "w",
        ) as f:
            json.dump(count_unique, f, indent=4, sort_keys=True)

    g.close()


def getUniqueEntriesInColumn(
    path,
    col,
    separator,
    skiprows=1,
    maxlines=1e10,
    printInterval=250000,
):
    """
    Count the occurance of unique codes in a column of a data file
    """
    #
    # Start processing
    #
    line = True
    iline = 0
    count = {}

    #
    # Process
    #
    with open(path, "r") as f:
        for ii in range(skiprows):
            line = f.readline()
        while line:
            #
            # Read
            #
            line = f.readline()
            if separator == "":
                data = line.split()
            else:
                data = line.split(separator)
            iline += 1

            #
            # Count
            #
            if len(data) > col:
                if data[col] in count.keys():
                    count[data[col]] += 1
                else:
                    count[data[col]] = 1

            #
            # Print progress
            #
            if iline % printInterval == 0:
                print("Processed {} lines".format(iline))

            #
            # Exit if too many lines have been processed
            #
            if iline > maxlines:
                break

    #
    # Print results
    #
    print("Found {} unique keys".format(len(list(count.keys()))))

    stats = []
    for key in sorted(list(count.keys())):
        stats.append("{}: {}".format(key, count[key]))
    print(formatList(stats))

    return count
