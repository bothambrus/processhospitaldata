#################################################################################
#   ProcessHospitalData                                                         #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

from processhospitaldata.output import formatList


def readHeader(fn, separator=";"):
    """
    Reads header line of file `fn`
    """
    #
    # Read
    #
    with open(fn, "r") as f:
        line = f.readline()

    #
    # Process
    #
    if separator == "":
        header = line.split()
    else:
        header = line.split(separator)

    #
    # Clean
    #
    for i in range(len(header)):
        header[i] = str(header[i].strip())

        #
        # Remove some unexpected characters
        #
        header[i] = header[i].lstrip("\ufeff")

    return header


def printHeader(header, ncol=4, wcol=30):
    """
    Formatted printing of header
    """
    print(formatList(header))
