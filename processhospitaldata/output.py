#################################################################################
#   ProcessHospitalData                                                         #
#   Copyright (C) 2022  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################


def formatList(strlist, ncol=4, wcol=30):
    """
    Format a list of strings to display
    """
    n = len(strlist)
    formatString = "{{:{}}}".format(wcol)

    #
    # Assemble string to print
    #
    line = ""
    for i in range(n):
        #
        # Format to fixed width
        #
        line += formatString.format(strlist[i][0:wcol])
        if i < n - 1:
            if i != 0 and (i + 1) % ncol == 0:
                #
                # Add line breaks
                #
                line += "\n"
            else:
                line += " "

    return line
