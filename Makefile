init:
	pip3 install -r requirements.txt

checkstyle:
	black --check processhospitaldata  
	black --check process 
	isort --profile black --diff processhospitaldata
	isort --profile black --diff process

style:
	isort --profile black processhospitaldata
	black processhospitaldata 
	black process

.PHONY: init

